/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.layout;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * This layout is quite the hoopy frood.
 * 
 * @author Art McBain
 *
 */
public class ContentAwareCardLayout extends CardLayout {
	private static final long serialVersionUID = 1L;

	protected final Map<Container, Map<Object, Component>> components = new HashMap<Container, Map<Object, Component>>();
	protected final Map<Container, Integer> current = new HashMap<Container, Integer>();

	private int linearSearch(Component[] components, Component comp) {
		for(int i = 0; i < components.length; i++) {
			if(components[i] == comp) return i;
		}
		return -1;
	}

	public void addLayoutComponent(Component comp, Object constraints) {
		Container parent = comp.getParent();
		synchronized(parent.getTreeLock()) {

			if(components.get(parent) == null) {
				components.put(parent, new HashMap<Object, Component>());
			}

			if(current.get(parent) == null) {
				current.put(parent, linearSearch(parent.getComponents(), comp));
			}

			Map<Object, Component> container = components.get(comp.getParent());
			container.put(constraints, comp);
			super.addLayoutComponent(comp, constraints);
		}
	}

	/**
	 * Gets the constraints associated with the given component.
	 * 
	 * @param comp whose constraints to return
	 * @return an Object or null if no constraints are associated with the given component
	 */
	public Object getConstraints(Component comp) {
		Container parent = comp.getParent();
		synchronized(parent.getTreeLock()) {

			if(components.containsKey(parent)) {

				Map<Object, Component> constraints = components.get(parent);
				Set<Entry<Object, Component>> entries = constraints.entrySet();
				for(Entry<Object, Component> constraint : entries) {

					if(constraint.getValue().equals(comp)) {
						return constraint.getKey();
					}
				}
			}

			return null;
		}
	}

	/**
	 * Gets the component that is currently showing for the given parent.
	 * 
	 * @param target from which get the currently showing component
	 * @return a Component or null if none currently showing for the given parent
	 */
	public Component getCurrentComponent(Container target) {
		synchronized(target.getTreeLock()) {
			if(current.get(target) != null) {
				return target.getComponent(current.get(target));
			}
			return  null;
		}
	}

	/**
	 * Gets the component that was added with the given parent using the given constraint.
	 * 
	 * @param target from which to get the component associated with the given constraints
	 * @param constraints used to look up the component
	 * @return a Component or null if none associated with the given constraints for the given parent
	 */
	public Component getLayoutComponent(Container target, Object constraints) {
		synchronized(target.getTreeLock()) {
			return components.get(target).get(constraints);
		}
	}

	public void next(Container parent) {
		synchronized(parent.getTreeLock()) {
			super.next(parent);

			if(current.get(parent) != null) {
				current.put(parent, (current.get(parent) + 1) % parent.getComponentCount());
			}
		}
	}

	public void previous(Container parent) {
		synchronized(parent.getTreeLock()) {
			super.previous(parent);

			if(current.get(parent) != null) {
				if(current.get(parent) == 0) {
					current.put(parent, parent.getComponentCount() - 1); 
				} else {
					current.put(parent, current.get(parent) - 1);
				}
			}
		}
	}

	public void removeLayoutComponent(Component comp) {
		Container parent = comp.getParent();
		synchronized(parent.getTreeLock()) {

			if(components.containsKey(parent)) {

				Map<Object, Component> constraints = components.get(parent);
				Set<Entry<Object, Component>> entries = constraints.entrySet();
				for(Entry<Object, Component> constraint : entries) {

					if(constraint.getValue().equals(comp)) {
						constraints.remove(constraint.getKey());

						if(current.get(parent) == linearSearch(parent.getComponents(), comp)) {
							super.removeLayoutComponent(comp);

							if(parent.getComponentCount() > 0) {
								if(current.get(parent) > 0) {
									current.put(parent, current.get(parent) - 1);
								}
							} else {
								current.put(parent, null);
							}
						} else {
							super.removeLayoutComponent(comp);
						}

						// Note: this behavior is depended upon in addLayoutComponent
						//       remember to change the code there if this is modified 
						if(constraints.isEmpty()) {
							components.remove(constraints);
						}

						return;
					}
				}
			}

			super.removeLayoutComponent(comp);
		}
	}

}
