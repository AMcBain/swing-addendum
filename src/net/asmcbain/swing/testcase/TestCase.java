/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.testcase;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Swing test-case template.
 * 
 * @author Art McBain
 *
 */
public class TestCase {

	public static final double RATIO = 1.61803399;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setPreferredSize(new Dimension((int)(500 * RATIO), 500));
				frame.setContentPane(getContentPane());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);

			}
		});
	}

	public static JComponent getContentPane() {
		JPanel panel = new JPanel();

		//

		return panel;
	}

}
