/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.components.treetable;

/**
 * Indicates a mutable TreeTableModel
 * 
 * @author Art McBain
 *
 */
public interface MutableTreeTableModel extends TreeTableModel {

	/**
	 * Adds the given node to the end of the model.
	 * 
	 * @param node to be added
	 */
	public void addNode(TreeTableNode<?> node);

	/**
	 * Removes the given node from the model.
	 * 
	 * @param node to be removed
	 */
	public void removeNode(TreeTableNode<?> node);

	/**
	 * Sets the column names for this model.
	 * 
	 * @param names of the columns
	 */
	public void setColumnNames(Object... names);

}
