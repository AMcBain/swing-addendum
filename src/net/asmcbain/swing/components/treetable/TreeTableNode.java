/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.components.treetable;

import java.util.Collection;

/**
 * A simple tree-related interface suitable for a simple TreeTable implementation.
 * 
 * @author Art McBain
 *
 */
public abstract class TreeTableNode<T extends TreeTableNode<T>> {

	protected T parent;

	/**
	 * Creates a TreeTableNode with the given parent.
	 * 
	 * @param parent
	 */
	protected TreeTableNode(T parent) {
		this.parent = parent;
	}

	/**
	 * Gets the depth of a node by moving up the tree until it finds a node with no parent.
	 * Simple implementations may wish to call this internally, more complex ones may implement
	 * their own behavior. 
	 * 
	 * @return the depth of a given node relative to its root node
	 */
	public static int getDepthViaTree(TreeTableNode<?> node) {
		int count = 0;
		while((node = node.getParent()) != null) count++;
		return count;
	}

	/**
	 * Gets the children of this node (if any).
	 * 
	 * @return a collection containing the children of this node
	 */
	public abstract Collection<? extends TreeTableNode<T>> getChildren();

	/**
	 * Gets the value of this TreeTableNode for the given column. 
	 * 
	 * @param column
	 * @return the value at the given column
	 */
	public abstract Object getColumnValue(int column);

	/**
	 * Returns the depth of this node from the root node, the root node being 0. The default
	 * implementation of this method calls TreeTableNode.getDepthViaTree(). More complex
	 * implementations may wish to override this with their own behavior.
	 * 
	 * @return the depth of this node relative to the root node
	 */
	public int getDepth() {
		return TreeTableNode.getDepthViaTree(this);
	}

	/**
	 * Gets the parent node of this TreeTableNode.
	 * 
	 * @return the parent of this node
	 */
	public final T getParent() {
		return parent;
	}

	/**
	 * Returns the type of this node. This can be useful for classes which do not directly
	 * extend this class, but rather implement it via anonymous inner classes, or where a
	 * single implementation might have multiple different types (such as folder, file, etc).
	 * 
	 * @return the type of this node
	 */
	public abstract Object getType();

	/**
	 * Returns whether this node has children or not. The default implementation of this
	 * method calls TreeTableNode.getChildren() and asks the size of the returned Collection.
	 * If null is returned, this method returns false. More complex implementations may wish
	 * to override this with their own behavior.
	 * 
	 * @return whether this node has children
	 */
	public boolean hasChildren() {
		Collection<? extends TreeTableNode<T>> collection = getChildren();
		return (collection != null)? collection.size() > 0 : false;
	}

}
