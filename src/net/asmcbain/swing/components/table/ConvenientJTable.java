/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.components.table;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.List;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import net.asmcbain.swing.SwingUtil;

/**
 * As the name implies, this JTable has convenience methods for some operations that should be
 * easy, but JTable by itself makes them harder, sometimes because of certain assumptions it
 * can't necessarily make.
 * <br><br>
 * Over time (and development), this class has grown to just include common usage patterns,
 * which has a side benefit of being able to update behavior for the entire application in
 * one location.
 * 
 * @author Art McBain
 * 
 */
public class ConvenientJTable extends JTable {
	private static final long serialVersionUID = 1L;

	private MutableTableModel model;
	private double[] columnWidths;

	/**
	 * Constructs a default ConvenienceJTable.
	 */
	public ConvenientJTable() {
		this(SwingUtil.createMutableTableModel(new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false; // NO, it's NOT! stupid default implementation :(
			}
		}));
	}

	/**
	 * Constructs a ConvenientJTable with the given TableModel.
	 * 
	 * @param model with which to back this table instance
	 */
	public ConvenientJTable(TableModel model) {
		setModel(model);
		setIntercellSpacing(new Dimension());
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getTableHeader().setReorderingAllowed(false);

		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				if(columnWidths != null) {
					Dimension tableDim = ConvenientJTable.this.getSize();
	
					double total = 0;
					for(int i = 0; i < getColumnModel().getColumnCount(); i++) {
						total += columnWidths[i];
					}
	
					for(int i = 0; i < getColumnModel().getColumnCount(); i++) {
						TableColumn column = getColumnModel().getColumn(i);
						column.setPreferredWidth((int) (tableDim.width * (columnWidths[i] / total)));
					}
				}
			}
		});
	}

	/**
	 * Adds a column with the given name.
	 * 
	 * @param name of the new column
	 */
	public void addColumn(final Object name) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.addColumn(name);
			}
		});
	}

	/**
	 * Adds a row to the table with the given data.
	 * 
	 * @param data to be added as a new row
	 */
	public void addRow(final Object... data) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.addRow(data);
			}
		});
	}

	/**
	 * Adds a row to the table with the given data.
	 * 
	 * @param data to be added as a new row
	 */
	public void addRow(List<? extends Object> data) {
		addRow(data.toArray());
	}

	/**
	 * Removes all the rows in this table.
	 */
	public void clearTable() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.setRowCount(0);
			}
		});
	}

	/**
	 * Marks the given row to be updated.
	 * 
	 * @param row to be updated
	 */
	public void markRowUpdated(final int row) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				if(row != -1) {
					model.fireTableRowsUpdated(row, row);
				}
			}
		});
	}

	/**
	 * Removes the given row from the table.
	 * 
	 * @param row to be removed
	 */
	public void removeRow(final int row) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				if(row != -1) {
					model.removeRow(row);
				}
			}
		});
	}

	/**
	 * Removes the selected row.
	 */
	public void removeSelectedRow() {
		int row = getSelectedRow();
		setSelectedRow((row == getRowCount() - 1)? row - 1 : row + 1);
		removeRow(row);
	}

	/**
	 * Selects the first row.
	 */
	public void selectFirstRow() {
		setSelectedRow(0);
	}

	/**
	 * Selects the last row.
	 */
	public void selectLastRow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				setSelectedRow(getRowCount() - 1);
			}
		});
	}

	/**
	 * Whether to allow the user to reorder columns.
	 * 
	 * @param value indicating whether to allow column reordering
	 */
	public void setAllowColumnReordering(boolean value) {
		getTableHeader().setReorderingAllowed(value);
	}

	/**
	 * Sets the given cell renderer for the whole table.
	 * 
	 * @param renderer to be used for all current columns
	 */
	public void setCellRenderer(final TableCellRenderer renderer) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				for(int i = 0; i < getColumnCount(); i++) {
					getColumn(getColumnName(i)).setCellRenderer(renderer);
				}
			}
		});
	}

	/**
	 * Makes the cell grid space go away.
	 */
	public void setCollapsedGrid() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				setShowGrid(false);
				setIntercellSpacing(new Dimension());
			}
		});
	}

	/**
	 * Sets the given data as the column names.
	 * 
	 * @param names of the columns
	 */
	public void setColumns(final Object... names) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.setColumnIdentifiers(names);
			}
		});
	}

	/**
	 * Sets the given data as the columns.
	 * 
	 * @param names of the columns
	 */
	public void setColums(List<? extends Object> names) {
		setColumns(names.toArray());
	}

	public void setModel(TableModel dataModel) {
		if (dataModel instanceof MutableTableModel) {
			model = (MutableTableModel)dataModel;
		}
		super.setModel(dataModel);
	}

	/**
	 * Sets the preferred widths of all the columns.
	 * 
	 * @param percentages of the total width to devote to columns
	 */
	public void setPreferredColumnWidths(double... percentages) {
		columnWidths = percentages;
	}

	/**
	 * Sets the given row to be selected.
	 * 
	 * @param row to be selected
	 */
	public void setSelectedRow(final int row) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				if(getRowCount() > 0) {
					getSelectionModel().setSelectionInterval(row, row);
				}
			}
		});
	}

	/**
	 * Marks the selected row to be updated.
	 */
	public void updateSelectedRow() {
		markRowUpdated(getSelectedRow());
	}

}

