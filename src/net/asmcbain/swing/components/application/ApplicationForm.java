/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.components.application;

/**
 * Interface for all ApplicationForms.
 * 
 * @author Art McBain
 *
 */
public interface ApplicationForm {

	/**
	 * Focus the first field on the form (if possible).
	 */
	public void focusForm();

	/**
	 * Resets the form to the initial defaults.
	 */
	public void resetForm();

}
