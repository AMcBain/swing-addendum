package net.asmcbain.swing.components.sheet;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBufferInt;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * Awesome drop-shadow creating panel by the listed author. 
 * 
 * @author Romain Guy
 * <br><b>website</b> - http://www.curious-creature.org/about/
 *
 */
// TODO merge shadow mask + blur in a single loop
public class DropShadowPanel extends JComponent {
	private static final long serialVersionUID = 1L;

	public static String KEY_BLUR_QUALITY = "blur_quality";
    public static String VALUE_BLUR_QUALITY_FAST = "fast";
    public static String VALUE_BLUR_QUALITY_HIGH = "high";
    
    protected BufferedImage shadow = null;
    protected BufferedImage original = null;

    protected float angle = 30;
    protected int distance = 5;

    protected int shadowSize = 5;
    protected float shadowOpacity = 0.5f;
    protected Color shadowColor = new Color(0x000000);

    // cached values for fast painting
    protected int distance_x = 0;
    protected int distance_y = 0;
    
    protected HashMap<Object, Object> hints;

    /**
     * Creates a default DropShadowPanel with no subject.
     */
    protected DropShadowPanel() {
        computeShadowPosition();
        hints = new HashMap<Object, Object>();
        hints.put(KEY_BLUR_QUALITY, VALUE_BLUR_QUALITY_FAST);
    }

    /**
     * Creates a DropShadowPanel with the given image resource as the subject.
     * 
     * @param imageName path to an image resource to be used as the shadow's subject
     */
    public DropShadowPanel(String imageName) {
        this();
        setSubject(imageName);
    }

    /**
     * Creates a DropShadowPanel with the image at the given URL.
     * 
     * @param imageUrl URL to an image to be used as the shadow's subject
     */
    public DropShadowPanel(URL imageUrl) {
        this();
        setSubject(imageUrl);
    }

    /**
     * Creates a DropShadowPanel with the image specified by the given File.
     * 
     * @param imageFile path to an image to be used as the drop shadow's subject
     */
    public DropShadowPanel(File imageFile) {
        this();
        setSubject(imageFile);
    }

    /**
     * Creates a DropShadowPanel with the given BufferedImage as the subject.
     * 
     * @param image to be used as the drop shadow's subject
     */
    public DropShadowPanel(BufferedImage image) {
        this();
        setSubject(image);
    }

    /**
     * Sets a rendering hint for the drop shadow.
     * 
     * @param hint rendering hint key
     * @param value of the rendering hint
     */
    // TODO use an enum?
    public void setRenderingHint(Object hint, Object value) {
        hints.put(hint, value);
    }
    
    @Override
    public Dimension getPreferredSize() {
        Dimension size;
        if (original == null) {
            size = new Dimension(50, 50);
        } else {
            size = new Dimension(original.getWidth() + (distance + shadowSize) * 2,
                                 original.getHeight() + (distance + shadowSize) * 2);
        }
        return size;
    }

    /**
     * Sets the subject of the drop shadow to the image at the specified resource path.
     * 
     * @param imageName the path to an image resource
     */
    public void setSubject(String imageName) {
        URL imageUrl = DropShadowPanel.class.getResource(imageName);
        setSubject(imageUrl);
    }

    /**
     * Sets the subject of the drop shadow to the image at the given URL.
     * 
     * @param imageUrl
     */
    public void setSubject(URL imageUrl) {
        if (imageUrl != null) {
            try {
                BufferedImage subject = ImageIO.read(imageUrl);
                setSubject(subject);
            } catch (IOException e) {
                this.original = null;
                this.original = null;
            }
        } else {
            this.original = null;
            this.original = null;
        }
    }

    /**
     * Sets the subject of the drop shadow to the image specified by the given File.
     * 
     * @param imageFile to be given a drop shadow
     */
    public void setSubject(File imageFile) {
        if (imageFile != null) {
            try {
                BufferedImage subject = ImageIO.read(imageFile);
                setSubject(subject);
            } catch (IOException e) {
                this.original = null;
                this.original = null;
            }
        } else {
            this.original = null;
            this.original = null;
        }
    }

    /**
     * Sets the subject of the drop shadow to the given BufferedImage.
     * 
     * @param subject to given a drop shadow
     */
    public void setSubject(BufferedImage subject) {
        if (subject != null) {
            this.original = subject;
            refreshShadow();
        } else {
            this.original = null;
            this.original = null;
        }
    }

    /**
     * Returns the angle of the drop shadow.
     * 
     * @return a float
     */
    public float getAngle() {
        return angle;
    }

    /**
     * Sets the angle of the drop shadow.
     * 
     * @param angle of the drop shadow
     */
    public void setAngle(float angle) {
        this.angle = angle;
        computeShadowPosition();
    }

    /**
     * Returns the offset of the drop shadow from the edge of the shadow's subject. 
     * 
     * @return an int
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Sets the offset of the drop shadow from the edge of the shadow's subject.
     * 
     * @param distance the offset
     */
    public void setDistance(int distance) {
        this.distance = distance;
        computeShadowPosition();
    }

    /**
     * Returns the drop shadow color.
     * 
     * @return a Color
     */
    public Color getShadowColor() {
        return shadowColor;
    }

    /**
     * Sets the color of the drop shadow.
     * 
     * @param shadowColor the color of the drop shadow
     */
    public void setShadowColor(Color shadowColor) {
        if (shadowColor != null) {
            this.shadowColor = shadowColor;
            refreshShadow();
        }
    }

    /**
     * Returns the opacity of the drop shadow.
     * 
     * @return a float
     */
    public float getShadowOpacity() {
        return shadowOpacity;
    }

    /**
     * Sets the opacity of the drop shadow.
     * 
     * @param shadowOpacity the opacity of the shadow
     */
    public void setShadowOpacity(float shadowOpacity) {
        this.shadowOpacity = shadowOpacity;
        refreshShadow();
    }

    /**
     * Returns the drop shadow's corner arc size.
     * 
     * @return an int
     */
    public int getShadowSize() {
        return shadowSize;
    }

    /**
     * Sets the size of the drop shadow's corner arc.
     * 
     * @param shadowSize offset of the drop shadow
     */
    public void setShadowSize(int shadowSize) {
        this.shadowSize = shadowSize;
        refreshShadow();
    }

    /**
     * Updates the shadow.
     */
    public void refreshShadow() {
        if (original != null) {
            shadow = createDropShadow(original);
        }
    }

    /**
     * Computes the shadow's position based on the angle.
     */
    private void computeShadowPosition() {
        double angleRadians = Math.toRadians(angle);
        distance_x = (int) (Math.cos(angleRadians) * distance);
        distance_y = (int) (Math.sin(angleRadians) * distance);
    }

    /**
     * Creates a new image to be shadowed based on the given image.
     * 
     * @param image to be copied
     * @return a BufferedImage
     */
    private BufferedImage prepareImage(BufferedImage image) {
        BufferedImage subject = new BufferedImage(image.getWidth() + shadowSize * 2,
                                                  image.getHeight() + shadowSize * 2,
                                                  BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = subject.createGraphics();
        g2.drawImage(image, null, shadowSize, shadowSize);
        g2.dispose();

        return subject;
    }

    /**
     * Creates a drop shadow for the given image.
     * 
     * @param image to be shadowed
     * @return the shadow image
     */
    private BufferedImage createDropShadow(BufferedImage image) {
        BufferedImage subject = prepareImage(image);

        if (hints.get(KEY_BLUR_QUALITY) == VALUE_BLUR_QUALITY_HIGH) {
            BufferedImage shadow = new BufferedImage(subject.getWidth(),
                                                     subject.getHeight(),
                                                     BufferedImage.TYPE_INT_ARGB);
            BufferedImage shadowMask = createShadowMask(subject);
            getLinearBlurOp(shadowSize).filter(shadowMask, shadow);
            return shadow;
        }

        applyShadow(subject);
        return subject;
    }

    /**
     * Applies a shadow to given image
     * 
     * @param image to be shadowed
     */
    private void applyShadow(BufferedImage image) {
        int dstWidth = image.getWidth();
        int dstHeight = image.getHeight();

        int left = (shadowSize - 1) >> 1;
        int right = shadowSize - left;
        int xStart = left;
        int xStop = dstWidth - right;
        int yStart = left;
        int yStop = dstHeight - right;

        int shadowRgb = shadowColor.getRGB() & 0x00FFFFFF;

        int[] aHistory = new int[shadowSize];
        int historyIdx = 0;

        int aSum;

        int[] dataBuffer = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        int lastPixelOffset = right * dstWidth;
        float sumDivider = shadowOpacity / shadowSize;

        // horizontal pass

        for (int y = 0, bufferOffset = 0; y < dstHeight; y++, bufferOffset = y * dstWidth) {
            aSum = 0;
            historyIdx = 0;
            for (int x = 0; x < shadowSize; x++, bufferOffset++) {
                int a = dataBuffer[bufferOffset] >>> 24;
                aHistory[x] = a;
                aSum += a;
            }

            bufferOffset -= right;

            for (int x = xStart; x < xStop; x++, bufferOffset++) {
                int a = (int) (aSum * sumDivider);
                dataBuffer[bufferOffset] = a << 24 | shadowRgb;

                // substract the oldest pixel from the sum
                aSum -= aHistory[historyIdx];

                // get the lastest pixel
                a = dataBuffer[bufferOffset + right] >>> 24;
                aHistory[historyIdx] = a;
                aSum += a;

                if (++historyIdx >= shadowSize) {
                    historyIdx -= shadowSize;
                }
            }
        }

        // vertical pass
        for (int x = 0, bufferOffset = 0; x < dstWidth; x++, bufferOffset = x) {
            aSum = 0;
            historyIdx = 0;
            for (int y = 0; y < shadowSize; y++, bufferOffset += dstWidth) {
                int a = dataBuffer[bufferOffset] >>> 24;
                aHistory[y] = a;
                aSum += a;
            }

            bufferOffset -= lastPixelOffset;

            for (int y = yStart; y < yStop; y++, bufferOffset += dstWidth) {
                int a = (int) (aSum * sumDivider);
                dataBuffer[bufferOffset] = a << 24 | shadowRgb;

                // substract the oldest pixel from the sum
                aSum -= aHistory[historyIdx];

                // get the lastest pixel
                a = dataBuffer[bufferOffset + lastPixelOffset] >>> 24;
                aHistory[historyIdx] = a;
                aSum += a;

                if (++historyIdx >= shadowSize) {
                    historyIdx -= shadowSize;
                }
            }
        }
    }

    /**
     * Creates a shadow mask for the given image.
     * 
     * @param image to be masked
     * @return a mask of the given image
     */
    private BufferedImage createShadowMask(BufferedImage image) {
        BufferedImage mask = new BufferedImage(image.getWidth(),
                                               image.getHeight(),
                                               BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = mask.createGraphics();
        g2d.drawImage(image, 0, 0, null);
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_IN,
                                                    shadowOpacity));
        g2d.setColor(shadowColor);
        g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
        g2d.dispose();

        return mask;
    }

    /**
     * Creates a ConvolveOp for the given blur size
     * 
     * @param size of blur
     * @return a ConvolveOp for applying a blur
     */
    private ConvolveOp getLinearBlurOp(int size) {
        float[] data = new float[size * size];
        float value = 1.0f / (float) (size * size);
        for (int i = 0; i < data.length; i++) {
            data[i] = value;
        }
        return new ConvolveOp(new Kernel(size, size, data));
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (shadow != null) {
            int x = (getWidth() - shadow.getWidth()) / 2;
            int y = (getHeight() - shadow.getHeight()) / 2;
            g.drawImage(shadow, x + distance_x, y + distance_y, null);
        }

        if (original != null) {
            int x = (getWidth() - original.getWidth()) / 2;
            int y = (getHeight() - original.getHeight()) / 2;
            g.drawImage(original, x, y, null);
        }
    }
}
