/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.example.components.application;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.components.application.AbstractApplicationWindow;
import net.asmcbain.swing.example.AggregateExample;
import net.asmcbain.swing.example.Example;
import net.asmcbain.swing.example.ExampleUtility;
import net.asmcbain.swing.layout.SingleCenteredItemLayout;

/**
 * An example that demonstrates classes from the application package.
 * 
 * @author Art McBain
 *
 */
@AggregateExample
public class ApplicationExample extends AbstractApplicationWindow implements Example {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				SwingUtil.setSystemLookAndFeel();

				new ApplicationExample();

			}
		});
	}


	static final String USER_NO_ACT = "No user action";
	static final String USER_ACCEPT = "User accepted dialog";
	static final String USER_CANCEL = "User canceled dialog";

	private ApplicationExample() {
		super("Application Examples");

		double ratio = 1.61803399d;
		int size = 400;
		frame.setPreferredSize(new Dimension((int)(size * ratio), size));

		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				if(!isDialogShowing()) {

					// A more complex application might throw out this event to
					// listeners to notify of closing instead of exiting
					if(JFrame.EXIT_ON_CLOSE ==
					          ExampleUtility.DEFAULT_EXAMPLE_CLOSE_OPERATION) {

						System.exit(0);

					} else if(JFrame.DISPOSE_ON_CLOSE ==
					          ExampleUtility.DEFAULT_EXAMPLE_CLOSE_OPERATION) {

						frame.dispose();
					}
				} else {
					frame.setExtendedState(JFrame.NORMAL);
				}
			}
		});


		/*
		 * Set up demo buttons to launch various types of dialogs
		 * 
		 * In order to simplify this example, some typical application
		 * code has been omitted. Ideally the GUI widgets here would send out
		 * to listeners, which would then interact with other application parts
		 * and talk back to the ApplicationWindowExample, instead of doing
		 * everything in the "view" as shown here. At least, that was the
		 * intended idea ...
		 */

		JPanel dialogActionsGrid = new JPanel(new GridLayout(1, 0, 8, 0));
		dialogActionsGrid.add(new SimpleDialogActionsPanel(this));
		dialogActionsGrid.add(new ComplexDialogActionsPanel(this));
		dialogActionsGrid.add(new DuplicateDialogActionsPanel(this));

		frame.setLayout(new SingleCenteredItemLayout());
		frame.add(dialogActionsGrid, SingleCenteredItemLayout.CENTER);


		// Action
		show();
	}

}
