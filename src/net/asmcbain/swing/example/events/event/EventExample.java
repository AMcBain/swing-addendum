/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.example.events.event;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Path2D;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.events.Event;
import net.asmcbain.swing.example.Example;
import net.asmcbain.swing.example.ExampleUtility;

/**
 * An example that demonstrates the {@link Event} class.
 * 
 * @author Art McBain
 *
 */
public class EventExample implements Example {

	private static final Logger LOGGER = Logger.getLogger(EventExample.class.getName());

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				SwingUtil.setSystemLookAndFeel();

				final JFrame frame = new JFrame("Event Propagation Example");
				frame.setDefaultCloseOperation(ExampleUtility.DEFAULT_EXAMPLE_CLOSE_OPERATION);
				frame.setResizable(false);


				/*
				 * Event listeners
				 */


				/* Frame Listener */

				final PropagationIndicator frameIndicator = new PropagationIndicator("Frame");

				final MouseAdapter frameAdapter = new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {

						// Update the GUI to indicate the event status
						if(Event.isCapturing()) {
							frameIndicator.setCaptured(true);
						}
						if(Event.isBubbling()) {
							frameIndicator.setBubbled(true);
						}

						// Cancel propagation for the proper phase if the GUI indicates
						if((!frameIndicator.shouldCapture() && Event.isCapturing()) ||
						          (!frameIndicator.shouldBubble() && Event.isBubbling())) {
							Event.stopPropagation();
						}
					}
				};
				frame.addMouseListener(frameAdapter);

				frameIndicator.setChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {

						// Remove or add the event listener as the GUI indicates
						if(frameIndicator.shouldEnable()) {
							frame.addMouseListener(frameAdapter);							
						} else {
							frame.removeMouseListener(frameAdapter);
						}
					}
				});


				/* Panel Listener */

				final PropagationIndicator panelIndicator = new PropagationIndicator("Content Pane");

				final MouseAdapter panelAdapter = new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {

						if(Event.isCapturing()) {
							panelIndicator.setCaptured(true);
						}
						if(Event.isBubbling()) {
							panelIndicator.setBubbled(true);
						}

						if((!panelIndicator.shouldCapture() && Event.isCapturing()) ||
						          (!panelIndicator.shouldBubble() && Event.isBubbling())) {
							Event.stopPropagation();
						}
					}
				};

				final JPanel contentPane = (JPanel)frame.getContentPane();
				contentPane.addMouseListener(panelAdapter);

				panelIndicator.setChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {

						if(panelIndicator.shouldEnable()) {
							contentPane.addMouseListener(panelAdapter);
						} else {
							contentPane.removeMouseListener(panelAdapter);
						}
					}
				});


				/* Button Listener */

				final TargetIndicator buttonIndicator = new TargetIndicator("Button");

				final JButton button = new JButton("Generate Click Event");
				button.addMouseListener(new MouseAdapter() {
					// Clear the GUI state
					public void mouseEntered(MouseEvent e) {
						frameIndicator.reset();
						panelIndicator.reset();
						buttonIndicator.reset();
					}
				});
				contentPane.add(button, BorderLayout.SOUTH);

				final MouseAdapter buttonAdapter = new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {

						try {

							// Start event propagation!
							Event.propagate(button, "getMouseListeners",
							          "mouseClicked", e, new Runnable() {
								public void run() {

									buttonIndicator.setActive(true);

									if(!buttonIndicator.shouldBubble()) {
										Event.stopPropagation();
									}
								}
							});
						} catch(Exception ex) {
							LOGGER.log(Level.WARNING, "event propagation failed", ex);
						}
					}
				};
				button.addMouseListener(buttonAdapter);

				buttonIndicator.setChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {

						if(buttonIndicator.shouldEnable()) {
							button.addMouseListener(buttonAdapter);
						} else {
							button.removeMouseListener(buttonAdapter);
						}
					}
				});


				/*
				 * Remaining GUI Setup
				 */

				JPanel indicatorPanel = new JPanel(new GridLayout(0, 1));
				indicatorPanel.add(frameIndicator);
				indicatorPanel.add(panelIndicator);
				indicatorPanel.add(buttonIndicator);
				frame.add(indicatorPanel, BorderLayout.CENTER);

				frame.setPreferredSize(new Dimension(300, 300));
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Class that displays propagation state for listeners on non-target objects
	 * 
	 * @author Art McBain
	 *
	 */
	private static class PropagationIndicator extends JPanel {
		private static final long serialVersionUID = 1L;

		private ChangeListener listener;

		private boolean captured;
		private boolean bubbled;

		private boolean shouldEnable = true;
		private boolean shouldCapture = true;
		private boolean shouldBubble = true;

		public PropagationIndicator(final String label) {
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createCompoundBorder(
			          BorderFactory.createEmptyBorder(0, 0, 1, 0),
			          BorderFactory.createLineBorder(Color.BLACK)
			));

			final int strw = SwingUtilities.computeStringWidth(
			          getFontMetrics(getFont()), label);
			final int strh = getFontMetrics(getFont()).getMaxAscent();

			JPanel activityPanel = new JPanel() {
				private static final long serialVersionUID = 1L;

				protected void paintComponent(Graphics g) {
					super.paintComponent(g);

					int width = getWidth();
					int height = getHeight() - 1;
					int lip = 5;
					int inset = 35;

					g.drawString(label, width / 2 - strw / 2, strh);

					if(captured) {
						Path2D path = new Path2D.Double();
						path.moveTo(lip + inset, 5);
						path.lineTo(lip + inset, height - 5 - inset);
						path.lineTo(inset, height - 5 - inset);
						path.lineTo(width / 4, height - 5);
						path.lineTo(width / 2 - inset, height - 5 - inset);
						path.lineTo(width / 2 - lip - inset, height - 5 - inset);
						path.lineTo(width / 2 - lip - inset, inset);
						path.lineTo(lip + inset, 5);
						g.setColor(new Color(255, 128, 128));
						((Graphics2D)g).fill(path);
						g.setColor(Color.RED);
						((Graphics2D)g).draw(path);
					}

					if(bubbled) {
						g = g.create(width / 2, 0, width, height + 1);
						((Graphics2D)g).rotate(Math.PI, width / 4, (height + 1) / 2);

						Path2D path = new Path2D.Double();
						path.moveTo(lip + inset, 5);
						path.lineTo(lip + inset, height - 5 - inset);
						path.lineTo(inset, height - 5 - inset);
						path.lineTo(width / 4, height - 5);
						path.lineTo(width / 2 - inset, height - 5 - inset);
						path.lineTo(width / 2 - lip - inset, height - 5 - inset);
						path.lineTo(width / 2 - lip - inset, inset);
						path.lineTo(lip + inset, 5);
						g.setColor(new Color(115, 230, 119));
						((Graphics2D)g).fill(path);
						g.setColor(Color.GREEN.darker());
						((Graphics2D)g).draw(path);
					}
				}
			};
			add(activityPanel, BorderLayout.CENTER);

			final JCheckBox capture = new JCheckBox("Stop Capture");
			capture.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					shouldCapture = !shouldCapture;
				}
			});

			final JCheckBox bubble = new JCheckBox("Stop Bubble");
			bubble.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					shouldBubble = !shouldBubble;
				}
			});

			JCheckBox active = new JCheckBox("Remove Listener");
			active.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					shouldEnable = !shouldEnable;
					capture.setEnabled(shouldEnable);
					bubble.setEnabled(shouldEnable);
					if(listener != null) {
						listener.stateChanged(new ChangeEvent(PropagationIndicator.this));
					}
				}
			});

			JPanel checkboxPanel = new JPanel(new GridLayout(0, 1));
			checkboxPanel.add(active);
			checkboxPanel.add(capture);
			checkboxPanel.add(bubble);
			add(checkboxPanel, BorderLayout.EAST);
		}

		public boolean shouldBubble() {
			return shouldBubble;
		}

		public boolean shouldCapture() {
			return shouldCapture;
		}

		public boolean shouldEnable() {
			return shouldEnable;
		}

		public void reset() {
			captured = false;
			bubbled = false;
			repaint();
		}

		public void setBubbled(boolean value) {
			bubbled = value;
			repaint();
		}

		public void setCaptured(boolean value) {
			captured = value;
			repaint();
		}

		public void setChangeListener(ChangeListener listener) {
			this.listener = listener;
		}

	}

	/**
	 * Class that displays the propagation state for listeners on the target object
	 * 
	 * @author Art McBain
	 *
	 */
	private static class TargetIndicator extends JPanel {
		private static final long serialVersionUID = 1L;

		private ChangeListener listener;

		private boolean active;

		private boolean shouldEnable = true;
		private boolean shouldBubble = true;

		public TargetIndicator(final String label) {
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createCompoundBorder(
			          BorderFactory.createEmptyBorder(0, 0, 1, 0),
			          BorderFactory.createLineBorder(Color.BLACK)
			));

			final int strw = SwingUtilities.computeStringWidth(
			          getFontMetrics(getFont()), label);
			final int strh = getFontMetrics(getFont()).getMaxAscent();

			JPanel activityPanel = new JPanel() {
				private static final long serialVersionUID = 1L;

				protected void paintComponent(Graphics g) {
					super.paintComponent(g);

					int width = getWidth();
					int height = getHeight() - 1;

					g.drawString(label, width / 2 - strw / 2, strh);

					g = g.create(0, 4, width, height);

					if(active) {
						g.setColor(new Color(128, 128, 255));
						g.fillArc(width / 2 - 25, height / 2 - 25, 50, 50, 0, 360);
						g.setColor(Color.BLUE);
						g.drawArc(width / 2 - 25, height / 2 - 25, 50, 50, 0, 360);
						g.setColor(getBackground());
						g.fillArc(width / 2 - 20, height / 2 - 20, 40, 40, 0, 360);
						g.setColor(Color.BLUE);
						g.drawArc(width / 2 - 20, height / 2 - 20, 40, 40, 0, 360);
						g.setColor(new Color(128, 128, 255));
						g.fillArc(width / 2 - 15, height / 2 - 15, 30, 30, 0, 360);
						g.setColor(Color.BLUE);
						g.drawArc(width / 2 - 15, height / 2 - 15, 30, 30, 0, 360);
						g.setColor(getBackground());
						g.fillArc(width / 2 - 10, height / 2 - 10, 20, 20, 0, 360);
						g.setColor(Color.BLUE);
						g.drawArc(width / 2 - 10, height / 2 - 10, 20, 20, 0, 360);
						g.setColor(new Color(128, 128, 255));
						g.fillArc(width / 2 - 5, height / 2 - 5, 10, 10, 0, 360);
						g.setColor(Color.BLUE);
						g.drawArc(width / 2 - 5, height / 2 - 5, 10, 10, 0, 360);
					}
				}
			};
			add(activityPanel, BorderLayout.CENTER);

			JCheckBox active = new JCheckBox("Remove Listener");
			active.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					shouldEnable = !shouldEnable;
					if(listener != null) {
						listener.stateChanged(new ChangeEvent(TargetIndicator.this));
					}
				}
			});

			JCheckBox propagate = new JCheckBox("Stop Bubble");
			propagate.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					shouldBubble = !shouldBubble;
				}
			});

			JPanel checkboxPanel = new JPanel(new GridLayout(0, 1));
			checkboxPanel.add(active);
			checkboxPanel.add(propagate);
			checkboxPanel.add(new JPanel());
			add(checkboxPanel, BorderLayout.EAST);
		}

		public boolean shouldBubble() {
			return shouldBubble;
		}

		public boolean shouldEnable() {
			return shouldEnable;
		}

		public void reset() {
			active = false;
			repaint();
		}

		public void setActive(boolean value) {
			active = value;
			repaint();
		}

		public void setChangeListener(ChangeListener listener) {
			this.listener = listener;
		}

	}

}

