/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.example.layout.compasslayout;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.example.Example;
import net.asmcbain.swing.example.ExampleUtility;
import net.asmcbain.swing.layout.CompassLayout;

/**
 * An example demonstrating the {@link CompassLayout} class.
 * 
 * @author Art McBain
 *
 */
public class CompassLayoutExample implements Example {

	public static final double RATIO = 1.61803399;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtil.setSystemLookAndFeel();

		ExampleUtility.launchExample("CompassLayout Example",
		          getContentPane(), SwingUtil.goldenDimension(500), false);
	}

	public static JPanel getContentPane() {
		JPanel panel = new JPanel(new GridLayout(0, 2));

		CompassLayout layout = new CompassLayout(5, 5);

		// CompassLayout with some missing components
		JPanel panel1 = new JPanel(layout);
		panel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel1.add(SwingUtil.createPanel(), CompassLayout.NORTH);
		panel1.add(SwingUtil.createPanel(125, 25), CompassLayout.NORTH_EAST);
		panel1.add(SwingUtil.createPanel(125, 25), CompassLayout.SOUTH_WEST);
		panel1.add(SwingUtil.createPanel(), CompassLayout.SOUTH);
		panel1.add(SwingUtil.createPanel(), CompassLayout.EAST);
		panel1.add(SwingUtil.createPanel(), CompassLayout.EAST_SOUTH);
		panel1.add(SwingUtil.createPanel(), CompassLayout.WEST_NORTH);
		panel1.add(SwingUtil.createPanel(), CompassLayout.WEST);
		panel1.add(SwingUtil.createPanel(), CompassLayout.CENTER);
		panel.add(panel1);

		// CompassLayout with all components and a padded line border
		JPanel panel2 = new JPanel(layout);
		panel2.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(Color.BLACK),
				BorderFactory.createEmptyBorder(1, 1, 1, 1)
		));
		panel2.add(SwingUtil.createPanel(125, 25), CompassLayout.NORTH_WEST);
		panel2.add(SwingUtil.createPanel(), CompassLayout.NORTH);
		panel2.add(SwingUtil.createPanel(125, 25), CompassLayout.NORTH_EAST);
		panel2.add(SwingUtil.createPanel(125, 25), CompassLayout.SOUTH_WEST);
		panel2.add(SwingUtil.createPanel(), CompassLayout.SOUTH);
		panel2.add(SwingUtil.createPanel(125, 25), CompassLayout.SOUTH_EAST);
		panel2.add(SwingUtil.createPanel(), CompassLayout.EAST_NORTH);
		panel2.add(SwingUtil.createPanel(), CompassLayout.EAST);
		panel2.add(SwingUtil.createPanel(), CompassLayout.EAST_SOUTH);
		panel2.add(SwingUtil.createPanel(), CompassLayout.WEST_NORTH);
		panel2.add(SwingUtil.createPanel(), CompassLayout.WEST);
		panel2.add(SwingUtil.createPanel(), CompassLayout.WEST_SOUTH);
		panel2.add(SwingUtil.createPanel(), CompassLayout.CENTER);
		panel.add(panel2);

		// CompassLayout showing only locations standard to BorderLayout
		JPanel panel3 = new JPanel(layout);
		panel3.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel3.add(SwingUtil.createPanel(), CompassLayout.NORTH);
		panel3.add(SwingUtil.createPanel(), CompassLayout.SOUTH);
		panel3.add(SwingUtil.createPanel(), CompassLayout.EAST);
		panel3.add(SwingUtil.createPanel(), CompassLayout.WEST);
		panel3.add(SwingUtil.createPanel(), CompassLayout.CENTER);
		panel.add(panel3);

		// CompassLayout with a center item that's stretched
		// (because it's a standard BorderLayout usage)
		JPanel panel4 = new JPanel(layout);
		panel4.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel4.add(SwingUtil.createPanel(), CompassLayout.CENTER);
		panel.add(panel4);

		return panel;
	}

}
