/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.example.layout.contentawarecardlayout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.example.Example;
import net.asmcbain.swing.example.ExampleUtility;
import net.asmcbain.swing.layout.ContentAwareCardLayout;

/**
 * An example demonstrating the {@link ContentAwareCardLayout} class.
 * 
 * @author Art McBain
 *
 */
public class ContentAwareCardLayoutExample implements Example {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtil.setSystemLookAndFeel();

		ExampleUtility.launchExample("ContentAwareCardLayout Example",
		          getContentPane());
	}

	private static JPanel getContentPane() {
		JPanel panel = new JPanel(new BorderLayout());


		/*
		 * Content Panels
		 */

		final JPanel trianglePanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				Polygon triangle = new Polygon(new int[] {
					0, getWidth() / 2, getWidth()	
				}, new int[] {
						getHeight(), 0, getHeight()
				}, 3);

				g.setColor(Color.RED);
				g.fillPolygon(triangle);
			}
		};

		final JPanel squarePanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				g.setColor(Color.GREEN);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
		};

		final JPanel circlePanel = new JPanel() {
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				g.setColor(Color.BLUE);
				g.fillArc(0, 0, getWidth(), getHeight(), 0, 360);
			}
		};


		/*
		 * Card Layout
		 */

		final ContentAwareCardLayout layout = new ContentAwareCardLayout();

		final JPanel cardPanel = new JPanel(layout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(75, 75, 75, 75));
		cardPanel.add(trianglePanel, "Triangle");
		cardPanel.add(squarePanel, "Square");
		cardPanel.add(circlePanel, "Circle");


		/*
		 * Control Area
		 */

		final JLabel panelLabel = new JLabel("", JLabel.CENTER);
		panelLabel.setText(layout.getConstraints(
		          layout.getCurrentComponent(cardPanel)).toString());

		final JButton previousButton = new JButton("Previous");
		previousButton.setEnabled(false);

		final JButton nextButton = new JButton("Next");

		previousButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Go back one and show constraint of card showing.
				layout.previous(cardPanel);
				panelLabel.setText(layout.getConstraints(
				          layout.getCurrentComponent(cardPanel)).toString());

				// Disable button when the first panel is reached.
				if(trianglePanel.equals(layout.getCurrentComponent(cardPanel))) {
					previousButton.setEnabled(false);
				} else {
					nextButton.setEnabled(true);
				}
			}
		});

		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Advance and show constraint of card showing.
				layout.next(cardPanel);
				panelLabel.setText(layout.getConstraints(
				          layout.getCurrentComponent(cardPanel)).toString());

				// Disable button when the last panel is reached.
				if(circlePanel.equals(layout.getCurrentComponent(cardPanel))) {
					nextButton.setEnabled(false);
				} else {
					previousButton.setEnabled(true);
				}
			}
		});

		JPanel controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(previousButton, BorderLayout.WEST);
		controlPanel.add(panelLabel, BorderLayout.CENTER);
		controlPanel.add(nextButton, BorderLayout.EAST);


		panel.setPreferredSize(
		          new Dimension(250, 250 + controlPanel.getPreferredSize().height));
		panel.add(cardPanel, BorderLayout.CENTER);
		panel.add(controlPanel, BorderLayout.SOUTH);

		return panel;
	}

}
