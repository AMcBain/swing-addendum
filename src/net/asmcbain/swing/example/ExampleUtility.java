/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.example;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * A utility class to aid in launching, discovering examples, and more.
 * 
 * @author Art McBain
 *
 */
public class ExampleUtility {

	private static final Logger LOGGER = Logger.getLogger(ExampleUtility.class.getName());

	/**
	 * The default operation to perform when closing example top-level frames.
	 */
	public static int DEFAULT_EXAMPLE_CLOSE_OPERATION = JFrame.EXIT_ON_CLOSE;

	/**
	 * Launches a frame with the given title and panel.
	 * 
	 * @param title for the new frame
	 * @param panel content pane for the new frame
	 */
	public static void launchExample(final String title, final JPanel panel) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				JFrame frame = new JFrame(title);
				frame.setDefaultCloseOperation(DEFAULT_EXAMPLE_CLOSE_OPERATION);
				frame.setContentPane(panel);
				frame.pack();
				frame.setMinimumSize(frame.getSize());
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Launches a frame with the given title, panel, and dimension.
	 * 
	 * @param title for the new frame
	 * @param panel content pane for the new frame
	 * @param dim dimension of the new frame
	 */
	public static void launchExample(final String title, final JPanel panel, final Dimension dim) {
		launchExample(title, panel, dim, true);
	}

	/**
	 * Launch a frame with the given title, panel, and dimension as the minimum
	 * frame size.
	 * 
	 * @param title for the new frame
	 * @param panel content pane for the new frame
	 * @param dim dimension of the new frame
	 * @param minimumSize whether the dimension should be treated as a minimum
	 * frame size
	 */
	public static void launchExample(final String title, final JPanel panel, final Dimension dim, final boolean minimumSize) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				JFrame frame = new JFrame(title);
				frame.setDefaultCloseOperation(DEFAULT_EXAMPLE_CLOSE_OPERATION);
				frame.setPreferredSize(dim);
				if(minimumSize) frame.setMinimumSize(frame.getPreferredSize());
				frame.setContentPane(panel);
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Launch a frame with the given title, panel, and dimension.
	 * 
	 * @param title for the new frame
	 * @param panel content pane for the new frame
	 * @param width of the new frame
	 * @param height of the new frame
	 */
	public static void launchExample(String title, JPanel panel, int width, int height) {
		launchExample(title, panel, new Dimension(width, height), true);
	}

	/**
	 * Launch a frame with the given title, panel, and dimensions as the minimum
	 * frame size.
	 * 
	 * @param title for the new frame
	 * @param panel content panel for the new frame
	 * @param width of the new frame
	 * @param height of the new frame
	 * @param minimumSize whether the dimensions should be treated as a minimum
	 * frame size
	 */
	public static void launchExample(String title, JPanel panel, int width, int height, boolean minimumSize) {
		launchExample(title, panel, new Dimension(width, height), minimumSize);
	}

	/**
	 * Lists all the example classes in this library.
	 * 
	 * @return a list of all the example classes in this library
	 */
	@SuppressWarnings("unchecked")
	public static List<Class<Example>> listExamples() {

		// TODO make this a separate class?

		InputStream input = ExampleUtility.class.getClass()
		          .getResourceAsStream("/META-INF/classes/" + Example.class.getName());

		if(input != null) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			try {
				List<Class<Example>> examples = new ArrayList<Class<Example>>();
				String line;

				while((line = reader.readLine()) != null) {
					examples.add((Class<Example>)Class.forName(line));
				}

				return examples;
			} catch(IOException e) {
				LOGGER.log(Level.WARNING, "error reading from classes file", e);
			} catch(ClassNotFoundException e) {
				LOGGER.log(Level.WARNING, "class in classes list does not exist", e);
			} finally {

				try {
					reader.close();
				} catch(IOException e) {
					LOGGER.log(Level.WARNING, "unable to close reader", e);
				}
			}
		}

		return null;
	}

}
