/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.swing.example;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.TreeNode;

import net.asmcbain.swing.SwingUtil;

/**
 * Finds all the example classes in this project and displays them in a
 * single window where users can run them easily.
 * 
 * @author Art McBain
 *
 */
public class ExampleLauncher {

	private static final Logger LOGGER = Logger.getLogger(ExampleLauncher.class.getName());

	/**
	 * @param args
	 */
	public static void main(final String[] args) {

		SwingUtil.setSystemLookAndFeel();
		ExampleUtility.DEFAULT_EXAMPLE_CLOSE_OPERATION = JFrame.DISPOSE_ON_CLOSE;

		int rows = 1;

		String examplePackage = ExampleLauncher.class.getPackage().getName() + ".";

		Map<String, ExamplePathTreeNode> map =
		          new HashMap<String, ExamplePathTreeNode>();

		ExamplePathTreeNode root = new ExamplePathTreeNode(null, "Examples");

		for(final Class<Example> example : ExampleUtility.listExamples()) {

			String className = example.getName().replace(examplePackage, "");
			String examplePath = className.substring(0, className.lastIndexOf("."));
			className = className.replace(examplePath + ".", "").replace("Example", "");

			int offset = 1;
			Annotation[] annotations = example.getAnnotations();
			for(int i = 0; i < annotations.length; i++) {
				if(annotations[i].annotationType().equals(AggregateExample.class)) {
					offset = 0;
					break;
				}
			}

			String[] paths = examplePath.split("\\.");
			for(int i = 0; i < paths.length - offset; i++) {

				if(!map.containsKey(paths[i])) {
					rows++;

					ExamplePathTreeNode parent = (i == 0)? root : map.get(paths[i - 1]);
					map.put(paths[i], new ExamplePathTreeNode(parent, paths[i]));
					parent.addChild(map.get(paths[i]));
				}
			}

			rows++;
			ExampleItemTreeNode item =
			          new ExampleItemTreeNode(map.get(paths[paths.length - 1 - offset]), className) {

			        	  public void run() {

								try {
									Method main = example.getDeclaredMethod("main",
									          String[].class);
									main.invoke(null, (Object)args);
								} catch(Exception e) {
									JOptionPane.showMessageDialog(null, e.getMessage(),
									          "Error", JOptionPane.ERROR_MESSAGE);
									LOGGER.log(Level.WARNING, "Unable to start example", e);
								}
			        	  }
			          };
			map.get(paths[paths.length - 1 - offset]).addChild(item);
		}

		final JTree tree = new JTree(root, true);
		tree.setSelectionModel(null);
		tree.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				
				if(e.getClickCount() >= 2) {
					ExamplePathTreeNode node = (ExamplePathTreeNode)tree
					          .getPathForLocation(e.getX(), e.getY())
					          .getLastPathComponent();

					if(node instanceof ExampleItemTreeNode) {
						((ExampleItemTreeNode)node).run();
					}
				}
			}
		});
		tree.setRowHeight(tree.getRowBounds(0).height);
		for(int i = 0; i < rows; i++) {
			tree.expandRow(i);
		}

		JScrollPane pane = new JScrollPane(tree);
		SwingUtil.setPreferredHeight(pane, rows * tree.getRowBounds(0).height + 2);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JLabel("Double-click an example to run it"), BorderLayout.NORTH);
		panel.add(pane, BorderLayout.CENTER);

		ExampleUtility.launchExample("Example Launcher", panel);
	}

	/**
	 * Example package paths
	 * 
	 * @author Art McBain
	 *
	 */
	private static class ExamplePathTreeNode implements TreeNode {

		private final ExamplePathTreeNode parent;
		private final String name;

		private final List<ExamplePathTreeNode> children =
		          new ArrayList<ExamplePathTreeNode>();

		/**
		 * Creates an ExamplePathTreeNode with the given parent and name.
		 * 
		 * @param parent of this node
		 * @param name of this node
		 */
		public ExamplePathTreeNode(ExamplePathTreeNode parent, String name) {
			this.parent = parent;
			this.name = name;
		}

		/**
		 * Adds the given node as a child.
		 * 
		 * @param child to be added
		 */
		public void addChild(ExamplePathTreeNode child) {
			children.add(child);
		}

		public Enumeration<ExamplePathTreeNode> children() {
			return Collections.enumeration(children);
		}

		public boolean getAllowsChildren() {
			return true;
		}

		public ExamplePathTreeNode getChildAt(int childIndex) {
			return children.get(childIndex);
		}

		public int getChildCount() {
			return children.size();
		}

		public int getIndex(TreeNode node) {
			return children.indexOf(node);
		}

		public TreeNode getParent() {
			return parent;
		}

		public boolean isLeaf() {
			return false;
		}

		public String toString() {
			return name;
		}
		
	}

	/**
	 * Example classes
	 * 
	 * @author Art McBain
	 *
	 */
	private static abstract class ExampleItemTreeNode extends ExamplePathTreeNode implements Runnable {

		/**
		 * Creates an ExampleItemTreeNode with the given parent and name.
		 * 
		 * @param parent of this node
		 * @param name of this node
		 */
		public ExampleItemTreeNode(ExamplePathTreeNode parent, String name) {
			super(parent, name);
		}

		public boolean getAllowsChildren() {
			return false;
		}

		public boolean isLeaf() {
			return true;
		}

	}

}
