/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.events;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * EventDispatcher is an implementation of the AbstractEventDispatcher. If dispatches are
 * needed to be "Swing safe," calls to the dispatch methods should be done on the EDT via the
 * appropriate EventQueue or SwingUtilities methods.
 * 
 * @author Art McBain
 *
 * @param <K> event type
 * @param <C> listener type
 */
public class EventDispatcher<K, C> extends AbstractEventDispatcher<K, C> {

	private static final Logger LOGGER = Logger.getLogger(EventDispatcher.class.getName());

	private final Object lock = new Object[0];

	protected final Map<K, List<C>> events;
	private final Map<K, Method> howto;
	private final TreeSet<K> first;

	/**
	 * Creates a default EventDispatcher.
	 */
	public EventDispatcher() {
		events = new HashMap<K, List<C>>();
		howto = new HashMap<K, Method>();
		first = new TreeSet<K>();
	}

	@SuppressWarnings("unchecked")
	public List<C> getEventListeners(K event) {
		return (events.get(event) != null)? Collections.unmodifiableList(events.get(event)) : (List<C>)Collections.EMPTY_LIST;
	}

	/**
	 * Adds the first public method of the specified listener class as the method to which to
	 * be dispatched events. This is useful for classes similar to Runnable which only have
	 * one declared public method.
	 * 
	 * @param event
	 */
	public void setDispatchMethod(K event) {
		synchronized(lock) {
			first.add(event);
			howto.remove(event);			
		}
	}

	/**
	 * Adds a method to be called on the listeners when a specific event is dispatched. No
	 * checks can be reasonably done (because of type erasure) so please ensure the given
	 * method is a Method obtained from the specified listener class.
	 * 
	 * @param event
	 * @throws IllegalArgumentException if the given method is null
	 */
	public void setDispatchMethod(K event, Method method) {
		synchronized(lock) {
			if(method == null) throw new IllegalArgumentException("Method cannot be null");
			first.remove(event);
			howto.put(event, method);
		}
	}

	/**
	 * Removes the specified dispatch method for the given event. This will cause all
	 * subsequent event dispatches for this event to fail unless a new method to which to
	 * dispatch is set.
	 * 
	 * @param event
	 */
	public void removeDispatchMethod(K event) {
		synchronized(lock) {
			first.remove(event);
			howto.remove(event);
		}
	}

	/**
	 * Dispatches the specified event with no parameters to all listeners registered for that
	 * event. If no listeners are registered for a given event, nothing is done. If there is
	 * no registered method to call for this event on the given listener, nothing will be
	 * dispatched.
	 * 
	 * @param event
	 */
	public boolean dispatchEvent(K event) {
		return dispatchEvent(event, (Object[])null);
	}

	/**
	 * Attempts to find a given method on the given class. Returns null if none found.
	 * 
	 * @param clazz
	 * @return
	 */
	private Method findMethod(Method method, Class<?> clazz) {
		for(Method m : clazz.getMethods()) {
			if(method.getName().equals(m.getName()) &&
					Arrays.equals(method.getParameterTypes(), m.getParameterTypes())) {
				return m;
			}
		}
		return null;
	}

	/**
	 * Dispatches the specified event with the given parameter values to all listeners
	 * registered for that event. If no listeners are registered for a given event, nothing is
	 * done. If there is no registered method to call for this event on the given listener,
	 * nothing will be dispatched.
	 * 
	 * @param event
	 * @param args
	 * @return
	 */
	public boolean dispatchEvent(K event, Object... args) {
		synchronized(lock) {
			try {
				Method method = howto.get(event);
				boolean fireFirst = (method == null && Boolean.TRUE.equals(first.contains(event)));
				if((method == null && !fireFirst)) {
					throw new NoSuchMethodException();
				}
	
				if(events.containsKey(event)) {
	
					List<C> listeners = events.get(event);
					for(C listener : listeners) {
	
						if(fireFirst) {
							// More work has to be done here to ensure we get the actual
							// method, allowing us to call it on other similar anonymous
							// instances without any issues
							if(listener.getClass().isAnonymousClass()) {
	
								if(listener.getClass().getMethods().length == 0) {
									throw new NoSuchMethodException();
								}
								method = listener.getClass().getMethods()[0];
	
								boolean found = false;
								if(listener.getClass().getSuperclass() != null) {
									Method m = findMethod(method, listener.getClass().getSuperclass());
									if(found = (m != null)) {
										method = m;
									}
								}
	
								if(!found && listener.getClass().getInterfaces().length > 0) {
									for(Class<?> i : listener.getClass().getInterfaces()) {
										Method m = findMethod(method, i);
										if(found = (m != null)) {
											method = m;
											break;
										}
									}
								}
	
							} else {
								method = listener.getClass().getMethods()[0];
							}
							howto.put(event, method);
							first.remove(event);
						}
	
						// This happens when calling to anonymous inner classes
						if(!Modifier.isPublic(method.getDeclaringClass().getModifiers())) {
							method.setAccessible(true);
						}
						method.invoke(listener, args);
					}
				}
			} catch(Exception e) {
				LOGGER.log(Level.WARNING, "Unable to dispatch event", e);
				return false;
			}

			return true;
		}
	}

	public void addEventListener(K event, C listener) {
		synchronized(events) {
			if(!events.containsKey(event)) {
				events.put(event, new LinkedList<C>());
			}
			events.get(event).add(listener);
		}
	}

	public boolean removeEventListener(K event, C listener) {
		synchronized(events) {
			if(events.containsKey(event)) {
				events.get(event).remove(listener);
				return true;
			}
		}
		return false;
	}

}
