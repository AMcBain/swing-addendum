/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.events;

/**
 * This facilitates listener-based dispatching as opposed to {@link EventInterface}'s
 * method-based dispatching. Instead of adding listeners per event, an entire
 * listener is added and set up to receive events specified for each method on the
 * given listener.
 * 
 * @author Art McBain
 *
 * @param <C> listener type
 */
public interface MultiEventInterface<C> {

	/**
	 * Registers the given listener for all the events its methods specify.
	 * The appropriate methods will be notified when an event it is registered for is
	 * dispatched.
	 * 
	 * @param listener
	 */
	public void addEventListener(C listener);

	/**
	 * Unregisters the given listener for all the events its methods specify.
	 * The appropriate methods will no longer be notified when an event it was
	 * registered for is dispatched.
	 * 
	 * @param listener
	 */
	public void removeEventListener(C listener);

}
