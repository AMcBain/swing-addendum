/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * https://asmcbain.net wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
package net.asmcbain.events;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

import net.asmcbain.events.EventDispatcher;

import org.junit.Before;
import org.junit.Test;


/**
 * Tests the EventDispatcher class.
 * 
 * @author Art McBain
 *
 */
public class TestEventDispatcher {

	public static interface Listener {
		public void tell(String msg);
	}

	private EventDispatcher<String, Runnable> runnableDispatcher;
	private EventDispatcher<String, Listener> listenerDispatcher;

	@Before public void setup() {
		runnableDispatcher = new EventDispatcher<String, Runnable>();
		runnableDispatcher.setDispatchMethod("run");
		runnableDispatcher.addEventListener("run", new Runnable() {
			public void run() {
				//System.out.println("run method called");
			}
		});
		listenerDispatcher = new EventDispatcher<String, Listener>();
		listenerDispatcher.setDispatchMethod("tell");
		listenerDispatcher.addEventListener("tell", new Listener() {
			public void tell(String msg) {
				//System.out.println("tell method called with: " + msg);
			}
		});
	}

	@Test public void TestNoArgumentSingleMethodInterfaceNoArguments() {
		assertTrue(runnableDispatcher.dispatchEvent("run"));
	}

	@Test public void TestNoArgumentSingleMethodInterfaceNullArgument() {
		assertTrue(runnableDispatcher.dispatchEvent("run", (Object[])null));
	}

	@Test public void TestNoArgumentSingleMethodInterfaceWithArguments() {
		assertFalse(runnableDispatcher.dispatchEvent("run", ""));
	}

	/*
	 * Before it was fixed, using the no-args setDispatchMethod would cause
	 * the dispatcher to grab the first method on the first object given.
	 * This caused it to grab a method from an instance of (for example)
	 * ClassName$1 and (possibly) later attempt to invoke it on an instance
	 * of ClassName$2, even though both might be anonymous inner instances
	 * of Runnable. Now it attempts to find the real declared method.  
	 */
	@Test public void TestAnonymousInnerClassWithFirstMethodCase() {
		runnableDispatcher.addEventListener("run", new Runnable() {
			public void run() {
				//System.out.println("run method called");
			}
		});
		assertTrue(runnableDispatcher.dispatchEvent("run"));
	}

	@Test public void TestSingleArgumentSingleMethodInterfaceWithNoArguments() {
		assertFalse(listenerDispatcher.dispatchEvent("tell"));
	}

	@Test public void TestSingleArgumentSingleMethodInterfaceWithNullArgument() {
		assertFalse(listenerDispatcher.dispatchEvent("tell", (Object[])null));
	}

	@Test public void TestSingleArgumentSingleMethodInterfaceWithArguments() {
		assertTrue(listenerDispatcher.dispatchEvent("tell", "A message"));
	}

	@Test public void TestSingleArgumentSingleMethodInterfaceWithArguments2() {
		listenerDispatcher.setDispatchMethod("tell", EventDispatcher.safeMethodLookup(Listener.class, "tell", String.class));
		assertTrue(listenerDispatcher.dispatchEvent("tell", "A message"));
	}

	@Test public void TestSingleArgumentsSingleMethodInterfaceWithArguments3() {
		listenerDispatcher.addEventListener("tell", new Listener() {
			public void tell(String msg) {
				//System.out.println("tell method called with: " + msg);
			}
		});
		assertTrue(listenerDispatcher.dispatchEvent("tell", "A message"));
	}

}

